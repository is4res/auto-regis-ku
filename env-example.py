from datetime import datetime

ENV = {
	"datetime": "2019-09-14 08:00:00", # set date time to start script (can use datetime.now().strftime("%Y-%m-%d %H:%M:%") to start immediately)
    "username": "",
    "password": "",
    "subjects": [
        {
            "course_id": "01234567",
            "lecture_sec": "1",
            "lab_sec": "1"
        },
        {
            "course_id": "01234567",
            "lecture_sec": "1"
        }
    ]
}
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from datetime import datetime
import time
import sys
from env import ENV
import sys

current_date = datetime.now()
start_date = datetime.strptime(ENV["datetime"], "%Y-%m-%d %H:%M:%S")
wait = current_date - start_date
time.sleep(wait.seconds)

if "win" in sys.platform:
    browser = webdriver.Chrome(executable_path=r"./chromedriver-win.exe")
elif "linux" in sys.platform:
    browser = webdriver.Chrome(executable_path=r"./chromedriver-linux")
elif "mac" in sys.platform:
    browser = webdriver.Chrome(executable_path=r"./chromedriver-mac")

error_msg = None

try:
    # login
    browser.get(("https://stdregis.ku.ac.th/_Login.php"))
    wait = WebDriverWait(browser, 10)
    form_username = wait.until(EC.presence_of_element_located((By.NAME, "form_username")))
    form_password = wait.until(EC.presence_of_element_located((By.NAME, "form_password")))
    form_username.send_keys(ENV["username"])
    form_password.send_keys(ENV["password"])
    submit_btn = wait.until(EC.element_to_be_clickable((By.XPATH, "//input[@type='submit']")))
    submit_btn.click()
    try:
        wait.until(EC.presence_of_element_located((By.XPATH, "//*[contains(text(), '" + ENV["username"] + "')]")))
    except:
        raise Exception('Fail to login.')

    # regis
    subjects = ENV["subjects"]
    index = 0
    retry_count = 0

    while len(subjects) > 0:
        subject = subjects[index]
        browser.get(("https://stdregis.ku.ac.th/_Student_Registration.php"))
        wait = WebDriverWait(browser, 10)

        try:
            # search subject
            cs_code = wait.until(EC.presence_of_element_located((By.NAME, "Cs_Code")))
            cs_code.send_keys(subject["course_id"])
            submit_search = wait.until(EC.presence_of_element_located((By.NAME, "submit")))

			# choose section and add
            try:
                lc_section = wait.until(EC.presence_of_element_located((By.NAME, "Lc_Section")))
                lc_section_option = EC.presence_of_element_located((By.XPATH, "//select[@name='Lc_Section']/option[text()='" + subject["lecture_sec"] + "']"))
                lc_section_option.click()
            except:
                continue

            try:
                lb_section = wait.until(EC.presence_of_element_located((By.NAME, "Lb_Section")))
                lb_section_option = EC.presence_of_element_located((By.XPATH, "//select[@name='Lb_Section']/option[text()='" + subject["lab_sec"] + "']"))
                lb_section_option.click()
            except:
                continue

            if lc_section or lb_section:
                go = wait.until(EC.presence_of_element_located((By.NAME, "go")))
                go.click()
        
            # add response
            try:
                wait.until(EC.presence_of_element_located((By.XPATH, "//*[contains(text(), 'บันทึกรายวิชาเรียบร้อยแล้ว')]")))
                del subjects[index]
            except:
                continue
                    
            if index > len(subjects) - 1:
                index = 0
                submit_search.click()
        except TimeoutException:
            retry_count += 1
            if retry_count > 10:
                raise Exception("Timeout, please try again later")
            print("Retry open register page: " + str(retry_count))
            time.sleep(10)

except TimeoutException:
    error_msg = "Timeout, please try again later"
except Exception as e:
    error_msg = e
finally:
    if error_msg is not None:
        print("\n----- Error -----")
        print(error_msg)

    browser.quit()